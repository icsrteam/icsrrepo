<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Justice For Jishnu</title>
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
   <!-- <link rel="stylesheet" href="css/main.css"> -->
    <link rel="stylesheet" href="css/lightgallery.css">
    <link rel="stylesheet" href="css/ninja-slider.css">
    <link rel="stylesheet" href="css/style.css"> 
</head>

<body>
    <header>
    <?php if($page_id==1){ ?>
            <div class="menu">
                <nav class="main_menu">
                    
                    <ul>
                        <li><a href="index.php" <?php if($page_id==1){ echo 'class="active"'; } ?>>HOME </a></li>
                        <li><a href="about-us.php" <?php if($page_id==2){ echo 'class="active"'; } ?>>ABOUT US </a></li>
                        <li><a href="support-for-students.php" class="service" <?php if($page_id==3){ echo 'class="active"'; } ?>>Support for Students</a></li>
                        <li><a href="support-for-colleges.php" <?php if($page_id==4){ echo 'class="active"'; } ?>>Support for Colleges</a></li>
                        <li><a href="support-us.php" <?php if($page_id==5){ echo 'class="active"'; } ?>>Support Us</a></li>
                        <li><a href="r&d.php" <?php if($page_id==6){ echo 'class="active"'; } ?>>CONTACT US</a></li>
                        <li><a href="career.php" <?php if($page_id==7){ echo 'class="active"'; } ?>>Manifesto</a></li>
                    </ul>
                </nav>
            </div>
    <?php } ?>
    
    <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6){ ?>
            <div class="menu inner_page_menu">
                <nav class="main_menu">
                    
                    <ul>
                        <li><a href="index.php" <?php if($page_id==1){ echo 'class="active"'; } ?>>HOME </a></li>
                        <li><a href="about-us.php" <?php if($page_id==2){ echo 'class="active"'; } ?>>ABOUT US </a></li>
                        <li><a href="support-for-students.php" <?php if($page_id==3){ echo 'class="active"'; } ?>>Support for Students</a></li>
                        <li><a href="support-for-colleges.php" <?php if($page_id==4){ echo 'class="active"'; } ?>>Support for Colleges</a></li>
                        <li><a href="support-us.php" <?php if($page_id==5){ echo 'class="active"'; } ?>>Support Us</a></li>
                        <li><a href="r&d.php" <?php if($page_id==6){ echo 'class="active"'; } ?>>CONTACT US</a></li>
                        <li><a href="career.php" <?php if($page_id==7){ echo 'class="active"'; } ?>>Manifesto</a></li>
                    </ul>
                </nav>
            </div>
    <?php } ?>
    
    </header>