    <footer id="footer">
        <div class="container">
            <div class="ft_logo">
                <img src="images/ft_logo.jpg">
            </div>
            <nav class="footer_menu">
                <ul>
                        <li><a href="index.php">HOME </a></li>
                        <li><a href="about-us.php">ABOUT US </a></li>
                        <li><a href="support-for-students.php">Support for Students</a></li>
                        <li><a href="support-for-colleges.php">Support for Colleges</a></li>
                        <li><a href="training-cell.php">Support Us</a></li>
                        <li><a href="r&d.php">CONTACT US</a></li>
                        <li><a href="career.php">Manifesto</a></li>
                    </ul>
            </nav>
            <hr>
            <div class="copyright">
                <div class="copy">
                    <p>Copyright © 2017. All Rights Reserved.</p>
                </div>
                <div class="powered"> <span>Powered by</span>
                    <a href="http://bodhiinfo.com/" target="_blank"><img src="images/bodhi.png" alt="bodhi"></a>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/ghost-typer.js"></script>
    <script src="js/footer-reveal.js"></script>
    <script src="js/main.js"></script>
    <script src="js/lightgallery.js"></script>
    <script src="js/ninja-slider.js"></script>
    <script src="js/stellarnav.min.js"></script>
    <script src="js/scripts.js"></script>