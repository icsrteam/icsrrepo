<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
<div class="about_page">
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
               <!-- <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>-->
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                   <h1>The Team</h1>
                    <div class="chapter_box">
                        <h2>Chapters</h2>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Calicut</h3>
                            <p>Raniyal Niyada</p>
                            <p>Nadeem K</p>
                            <p>Ajay Ratnan</p>
                            <p>Ashwin Venugopal Menon</p>
                            <p>Ebin Emerson</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Kochi</h3>
                            <p>Arjun PK</p>
                            <p>Reshma Suresh</p>
                            <p>Harima Hariharan</p>
                            <p>Sreenath MN</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <h3>Thiruvananthapuram</h3>
                            <p>Jojimon Jaseenthan</p>
                        </div>
                    </div>    
                </div>
                <div class="row" data-aos="fade-right">
                    <div class="chapter_box">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                                <h3>Advocates’ Panel</h3>
                                <p>Adv. Manu Sebastian</p>
                                <p>Adv. Surya Binoy</p>
                                <p>Adv. Legith R</p>
                                <p>Adv. Kenneth Joe Cleetus</p>
                        </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Advisory Panel</h3>
                                <p>Dr. VR Jayadevan, Professor, NUALS</p>
                    </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
    
</div>
   <?php include('includes/footer.php'); ?>
    <script>
            /*$("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            $('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>