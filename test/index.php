<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
            <center>
                <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>
            </center>
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction">
        <div class="container">
            <div class="content_box" data-aos="fade-up">
                <div class="we_are">
                   <h1>We are Standing for Justice</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis. Maecenas gravida blandit tellus, sit amet pretium nisi ornare sit amet. Nullam rutrum, felis sed tempor consequat, justo orci vulputate ipsum, in pretium sapien lectus sit amet nisl. Maecenas at purus sit amet nibh.</p>
                </div>
            </div>
        </div>
    </div>
<div class="buttons_area">
        <div class="container">
            <div class="button_group">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box help">
                            <a href="#">
                                <span>
                                    Seek Help
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box rights">
                            <a href="#">
                                <span>
                                    Know Your Rights
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box who_help">
                            <a href="#">
                                <span>
                                    Who can help you
                                </span>        
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box manifesto">
                            <a href="#">
                                <span>
                                    Manifesto
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="whats_new" id="main" role="main">
        <div class="container">
                <h1>What’s New?</h1>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="wht_box ">
                    <h3># Justice for Jishnu campaign</h3>
                    <img src="images/jishnu.jpg" alt="#justice For Jishnu">
                    <span>30-01-2017</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis. Maecenas gravida blandit tellus, sit amet pretium nisi ornare sit amet.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="wht_box flexslider carousel">
                    <h3># Justice for Jishnu campaign</h3>
                    <img src="images/jishnu.jpg" alt="#justice For Jishnu">
                    <span>30-01-2017</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis. Maecenas gravida blandit tellus, sit amet pretium nisi ornare sit amet.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="campaign_area">
        <div class="demo-gallery">
                    <div id="slide-gallery" class="list-unstyled">
                        <div class="row" style="background: #bf0404;">
                        <div class="col-lg-4 col-md-4 col-sm-4 mobile_gallery">
                            <div class="gallery_box">
                                <div class="gallery_padd">
                                    <div class="border">
                                        <h2>Our Campaigns</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery cam_img_1" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 mobile_gallery cam_img_2" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery cam_img_3" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                       <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 mobile_gallery cam_img_4" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery" data-src="images/39938853-bridge-wallpapers.jpg">
                            <div class="gallery_box">
                                <a href="#">
                                    <div class="gallery_padd know_mor">
                                        <div class="border">
                                            <h3>Know More</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
    </div>
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
   <?php include('includes/footer.php'); ?>
    <script>
            $("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            /*$('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>