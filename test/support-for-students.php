<?php 
	$page_id=3;
	include('includes/header.php'); 
?>
<div class="about_page supp_stud">
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
               <!-- <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>-->
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction">
        <div class="container">
            <div class="content_box" data-aos="fade-left">
                <div class="we_are">
                   <h1>Know your Rights</h1>
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>Can our colleges withhold our original certificates submitted during our admission?</span>
                    <p>According to AICTE Regulations (Establishment of of Mechanism for Grievance Redressal) Regulations, 2012, rule 2 f (iv) -  withholding or refusing to return any document in the form of certificates of degree, diploma or any other award or other document deposited with it by a person for the purpose of seeking admission in such institution, with a view to induce or compel such person to pay any fee or fees in respect of any course or programme of study which such person does not intend to pursue; constitutes a 'grievance', This is the same as under UGC (Grievance Redressal) Regulations, 2012., Rule 2 f (vi). <br>
                        <br>
                    If your college takes such an action, then a complaint can be made to the grievance redressal committee in your college first, and if adequate action is not being taken (which is expected, of course), you can approach the University level committee. You can also file a complaint online directly to the UGC at <a href="http://ugc.ac.in/grievance/" target="_blank">ugc.ac.in/grievance</a>
                    </p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>What can I do if there are threats of reducing marks, not giving attendance etc?</span>
                    <p>According to UGC Guidelines on adoption of Choice based credit system,  Section 6.2 holds that the assessment of the student has to be fair. Further, the Grievance redressal mechanism (as under the UGC or AICTE) are to consider "non transparent or unfair evaluation practices" as a grievance as under Rule 2 f (xiv). So, first you need to approach the Grievance Redressal Committee of your college and if your grievance is not settled, you may approach the University – level committee.</p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>Which all institutions come under  RTI?</span>
                    <p>All Public Authorities come under the ambit of RTI, under Section 2 clause h. In case of individual private colleges, one can file an RTI with the affiliating University to get relevant documents and the University may direct the respective college to provide the same. </p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>What is the legal position in Kerala regarding Campus Politics?</span>
                    <p>As per the Judgement of the Kerala High Court in Sojan Francis v. MG University, political activities  such as mass boycotting of classes, strike etc can be restricted inside the campus. However, the students have a right to representation, and the right to have an elected student body.<br>
                    <br>
                    College Unions and elections to the same have been declared to be mandatory for all colleges in India by the Hon’ble Supreme Court vide the order dated 22nd September 2006, enforcing the recommendations of the Lyngdoh Committee, constituted by the Hon’ble Court. This right of the students has been reiterated by the UGC in its Guidelines for Student Entitlement. Clauses 6 and 7 of the Guidelines read as follows
                    </p>
                    <p style="font-style: italic;">
                        6. As democratic citizens, The students are entitled to freedom of thought and expression within and outside their institution. The college/university must allow space for free exchange of ideas and public debate so as to foster a culture of critical reasoning and questioning. College/university authorities must not impose unreasonable, partisan or arbitrary restrictions on organizing seminars, lecture and debates that do not otherwise violate any law.<br>

<br>
7. The students are entitled to forming associations and unions, directly electing their representatives to Students Unions and having their representatives on the college/university decision making bodies including internal quality assessment, grievance committees, Gender Sensitization Committees against Sexual Harassment and the Academic/Executive council. University/colleges shall evolve mechanisms for adequate consultations with students’ representatives before taking any major decision affecting the students.<br>
<br>

Regarding the application of the same, it says <br>
“These guidelines apply to all the colleges and universities in the country (this expression includes every institutions of higher education even if it is not called college/university) without any exception.” <br><br>


And that <br>
“Any serious or persistent violation of these Guidelines can be brought to the notice of the University Grants Commission and can be the basis of punitive action against the offender.” 
<br><br>
                        
                        
                    </p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>I’m fined for keeping a beard. What can I do?</span>
                    <p>The college may enforce a code of conduct and require you to be in uniforms. However, unreasonable infringement of your fundamental rights is not allowed by our constitution. You can always challenge them in court. Collecting fines for ‘offences’ is also illegal if they haven’t specified it in your prospectus. You can bring this to attention of Kerala Admission Advisory and Fee Regulatory Committee.</p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>I am physically harassed in my College. What can I do?
</span>
                    <p>This is a serious issue and can be dealt with under the Indian Penal Code. If they have caused you any bodily pain, disease or infirmity, it amounts to the offence of ‘hurt’ defined under S.319 IPC and is liable to be punished for a jail term of up to 1 year and shall be liable to pay a fine. If they had used any dangerous weapon while assaulting you, the jail term will extend to 3 years. The punishment for voluntarily causing grievous hurt is imprisonment for a term which may extend to seven years, and fine. Threatening to cause harm you is also punishable, and torture is punishable under various provisions of the IPC. You need to approach the nearby police station and file a complaint, understand – they’re there to protect you, not only for slapping a fine on you for not wearing helmet.</p>
                    
                    <span><i class="fa fa-dot-circle-o" aria-hidden="true"></i>I am not allowed to leave the campus during class hours for any purposes. Is that justifiable? 

</span>
                    <p>Only reasonable restrictions can be made to freedom of movement, which is your fundamental right according to Article 19 (1) (d) of our constitution. There can be reasonable restrictions, but authorities cannot compel a student to attend class. That is the prerogative of the Student. He/she may face the consequences such as losing attendance etc, however this does not give the authorities any right to lock you unreasonably inside the campus. This can even amount to the offences of wrongful confinement, wrongful restraint etc which are punishable under IPC. The punishment can extend upto one year of jail term and fine of 1000 Rs.  
<br><br>

Do you have more questions? Ask us! We shall answer you, and also others who might have similar doubt!  (link to google form) 
</p>
                    
                </div>
                <a href="#">View All</a>
            </div>
        </div>
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                    <div class="chapter_box seek_help">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="border">
                                <h3>Seek our help</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                <a href="#" data-toggle="modal" data-target="#myModal">Fill the Fourm</a>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="whats_new impact">
                <div class="wht_box">
                    <h1>Who can help you?</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <a href="#">Know More</a>
                </div>
        </div>
    <div class="whats_new policy">
                <div class="wht_box">
                    <h1>Policy &amp; Research</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <a href="#">Know More</a>
                </div>
    </div>
    <!--<div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                   <h1>Newsroom</h1>
                    <div class="chapter_box news">
                        
                        
                        <div id="ninja-slider">
                            <div class="slider-inner">
                                <ul>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="fs-icon" title="Expand/Close"></div>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>-->
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
    
</div>
   <?php include('includes/footer.php'); ?>


<!-- Modal -->
  <div class="modal fade help_form" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <!-- <h4 class="modal-title">For our Help</h4>-->
            <p>Please Fill This Fourm</p>
        </div>
        <div class="modal-body help_filed">
          
                    <form>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit" class="btn btn-info">Send</button>
                    </form>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


    <script>
            /*$("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            $('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>