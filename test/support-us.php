<?php 
	$page_id=5;
	include('includes/header.php'); 
?>
<div class="about_page supp_stud supp_colle supp_us">
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><!--<img src="images/logo.png" alt="">--></a>
            </div>
               <!-- <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>-->
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                    <div class="chapter_box seek_help donation">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="border">
                                <h3>Offer a Donation</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                <a href="#" data-toggle="modal" data-target="#myModal">Fill the Fourm</a>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="whats_new impact">
                <div class="wht_box">
                    <h1>How your money helps</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <!--<a href="#">Know More</a>-->
                </div>
        </div>
    <div class="whats_new policy">
                <div class="wht_box">
                    <h1>Volunteer with Us</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <a href="#" data-toggle="modal" data-target="#myModal1">Volunteer with Us</a>
                </div>
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box coprate_supp" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                   <h1>Corporate Support</h1>
                    <div class="chapter_box news ">
                        
                        
                        <div id="ninja-slider">
                            <div class="slider-inner">
                                <ul>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg43 col-md43 col-sm-4">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg43 col-md43 col-sm-4">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg43 col-md43 col-sm-4">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="fs-icon" title="Expand/Close"></div>
                            </div>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
    
</div>
   <?php include('includes/footer.php'); ?>


<!-- Modal -->
  <div class="modal fade help_form" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <!-- <h4 class="modal-title">For our Help</h4>-->
            <p>Please Fill This Fourm</p>
        </div>
        <div class="modal-body help_filed">
          
                    <form>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit" class="btn btn-info">Send</button>
                    </form>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal -->
  <div class="modal fade help_form" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <!-- <h4 class="modal-title">For our Help</h4>-->
            <p>Please Fill This Fourm</p>
        </div>
        <div class="modal-body help_filed">
          
                    <form>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit" class="btn btn-info">Send</button>
                    </form>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


    <script>
            /*$("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            $('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>