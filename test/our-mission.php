<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
<div class="about_page">
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
               <!-- <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>-->
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction">
        <div class="container">
            <div class="content_box" data-aos="fade-left">
                <div class="we_are">
                   <h1>Our Mission</h1>
                    <p>We are a bunch of lawyers and law students on a mission - a mission to educate students about the rights that they hold and how they can exercise them. In the midst of all the private college commotion that’s going on, we came to realize that the existence of a body destined to teach them the laws that were made to safeguard their future, their identity as a free and grown up individual, was absolutely necessary. Towards this goal we have formed this pan-Kerala network to assist anyone in need of legal help to tackle the problems she/he faces from any college. <br>
                        <br>
                    We live in a constitutional democracy and we have certain rights which are protected by law. The regulators including UGC and AICTE have fabricated certain policies and laws that assist students in fortifying their rights and liberties. Colleges and the bodies that govern them have kept these laws under covers, hoping that no student will ever find them. We have found out that the failure of students in reporting the grievous incidents in colleges comes out of their fear for their future. <br>
                        <br>
                    Even many parents have kept a shut-eye towards these problems as they were worried about the legal complications. This is what we aim to eliminate. This is the problem that we are bound to rectify.<br>
                        <br>
                    Students, Parents and teachers, we have set our focus on being that window that you can use to know the rights you have. We will be actively accepting complaints and will help you find the solutions for the same. In addition to the Legal Aid/ Literacy initiatives, we will also contribute actively into policymaking through our research initiatives.  And we hope that no other student will ever be exposed to a treatment that is inhuman in nature by any college.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
    
</div>
   <?php include('includes/footer.php'); ?>
    <script>
            /*$("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            $('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>