<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
<div class="about_page">
    <div class="banner">
        <div class="container">
            
            <div class="logo">
                <a href="index.php"><img src="images/logo.jpg" alt=""></a>
            </div>
               <!-- <div class="text caption">
                    <h1># Justice for</h1>
                    <h2 id="ghost"></h2>
                </div>-->
        </div>
       <!-- <div id="banner_wrap"></div>-->
    </div>
    <div class="introduction">
        <div class="container">
            <div class="content_box" data-aos="fade-left">
                <div class="we_are">
                   <h1>Our Mission</h1>
                    <p>We are a bunch of lawyers and law students on a mission - a mission to educate students about the rights that they hold and how they can exercise them. In the midst of all the private college commotion that’s going on, we came to realize that the existence of a body destined to teach them the laws that were made to safeguard their future, their identity as a free and grown up individual, was absolutely necessary. Towards this goal we have formed this pan-Kerala network to assist anyone in need of legal help to tackle the problems she/he faces from any college. <br>
                        <br>
                    We live in a constitutional democracy and we have certain rights which are protected by law. The regulators including UGC and AICTE have fabricated certain policies and laws that assist students in fortifying their rights and liberties. Colleges and the bodies that govern them have kept these laws under covers, hoping that no student will ever find them. We have found out that the failure of students in reporting the grievous incidents in colleges comes out of their fear for their future. <br>
                        <br>
                    Even many parents have kept a shut-eye towards these problems as they were worried about the legal complications. This is what we aim to eliminate. This is the problem that we are bound to rectify.<br>
                        <br>
                    Students, Parents and teachers, we have set our focus on being that window that you can use to know the rights you have. We will be actively accepting complaints and will help you find the solutions for the same. In addition to the Legal Aid/ Literacy initiatives, we will also contribute actively into policymaking through our research initiatives.  And we hope that no other student will ever be exposed to a treatment that is inhuman in nature by any college.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                   <h1>Our Chapters</h1>
                    <div class="chapter_box">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <img src="images/slid_1.jpg">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Chapters 1</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                        </div>
                    </div>    
                </div>
                    <div class="row" data-aos="fade-right">
                    <div class="chapter_box">
                        
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Chapters 2</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                        </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <img src="images/slid_1.jpg">
                    </div>
                    </div>    
                </div>
                    <div class="row" data-aos="fade-left">
                    <div class="chapter_box">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <img src="images/slid_1.jpg">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                            <h3>Chapters 3</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                        </div>
                    </div>    
                </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="campaign_area">
        <div class="demo-gallery">
                    <div id="slide-gallery" class="list-unstyled">
                        <div class="row" style="background: #8bc34a;">
                        <div class="col-lg-4 col-md-4 col-sm-4 mobile_gallery">
                            <div class="gallery_box">
                                <div class="gallery_padd">
                                    <div class="border">
                                        <h2>Our Campaigns</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery cam_img_1" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 mobile_gallery cam_img_2" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery cam_img_3" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                       <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 mobile_gallery cam_img_4" data-src="images/-Clausura-CU-w-pencils-1.jpg">
                            <div class="gallery_box">
                                <a href="" class="hover"> <img class="img-responsive" src="">
                                    <div class="overlay over_show">
                                        <div class="border">
                                            <h4>Campaigns</h4> <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sed dolor sit amet magna interdum facilisis.</span> </div>
                                        </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 mobile_gallery" data-src="images/39938853-bridge-wallpapers.jpg">
                            <div class="gallery_box">
                                <a href="#">
                                    <div class="gallery_padd know_mor">
                                        <div class="border">
                                            <h3>Know More</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
    </div>
    
    

    <div class="whats_new impact">
                <div class="wht_box">
                    <h1>Our Impact</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <a href="#">Know More</a>
                </div>
        </div>
    <div class="whats_new policy">
                <div class="wht_box">
                    <h1>Policy &amp; Research</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                    <a href="#">Know More</a>
                </div>
    </div>
    <div class="introduction chapter">
        <div class="container">
            <div class="content_box" >
                <div class="we_are">
                
                <div class="row" data-aos="fade-left">
                   <h1>Newsroom</h1>
                    <div class="chapter_box news">
                        
                        
                        <div id="ninja-slider">
                            <div class="slider-inner">
                                <ul>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="fs-icon" title="Expand/Close"></div>
                            </div>
                        </div>
                    </div>    
                </div>
                <!--<div class="row" data-aos="fade-right">
                   <h1>Press Notes</h1>
                    <div class="chapter_box news">
                        
                        
                        <div id="ninja-slider_1">
                            <div class="slider-inner">
                                <ul>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="fs-icon" title="Expand/Close"></div>
                            </div>
                        </div>
                    </div>    
                </div>-->
                <!--<div class="row" data-aos="fade-left">
                   <h1>In the Media</h1>
                    <div class="chapter_box news">
                        
                        
                        <div id="ninja-slider_2">
                            <div class="slider-inner">
                                <ul>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="content">
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="images/slid_1.jpg">
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <h3>Unrestricted Content</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nulla tellus, mattis in suscipit eu, malesuada ut quam. Nullam a tellus et magna luctus vestibulum vel eu ante. Suspendisse pulvinar augue sit amet ultrices semper. Nunc id tempus turpis. Aliquam fermentum consectetur arcu vitae viverra. Nunc quis odio dui. Cras viverra ac ipsum a pretium. Pellentesque venenatis felis sit amet est scelerisque mollis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc vitae dui libero. Phasellus malesuada nunc eget cursus feugiat. Duis ornare porttitor molestie. Curabitur blandit elit quis commodo accumsan. Vestibulum blandit finibus orci.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="fs-icon" title="Expand/Close"></div>
                            </div>
                        </div>
                    </div>    
                </div>-->
                </div>
            </div>
        </div>
    </div>
    <!--<div class="buttons_area">
        <div class="container">
            <div class="button_group">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box help">
                            <a href="#">
                                <span>
                                    Seek Help
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box rights">
                            <a href="#">
                                <span>
                                    Know Your Rights
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box who_help">
                            <a href="#">
                                <span>
                                    Who can help you
                                </span>        
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="button_box manifesto">
                            <a href="#">
                                <span>
                                    Manifesto
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <div class="send_message_tab">
        <div class="send_box_bg">
            <div class="container">
                <div class="send_message">
                    <form>
                        <h3>Send Us a Message</h3>
                        <input type="text" placeholder="Name">
                        <input type="text" placeholder="Email">
                        <input type="text" placeholder="Phone">
                        <input type="text" placeholder="Subject">
                        <textarea placeholder="Your Message..."></textarea>
                        <button type="submit">Send</button>
                    </form>
                </div>
                <div class="click_mess">Send Us a Message</div>
            </div>
        </div>
    </div>
    
</div>
   <?php include('includes/footer.php'); ?>
    <script>
            /*$("#ghost").ghosttyper({
                messages: ['Jishnu', 'Students']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 6000
            });
            $('#slide-gallery').lightGallery();*/
        
    </script>
</body>

</html>